cmake_minimum_required(VERSION 2.6)
project(zetaplop)

include_directories("include")
add_subdirectory("src")