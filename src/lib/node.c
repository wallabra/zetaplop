#include "zplop/node.h"


void ZB_PathNodeList_Advancer(ZB_LinkedListView *list, ZB_LinkedListIterator *iter) {
    iter->listCurrent = (void*) ( ((ZB_PathNodeList*)iter->listCurrent)->next );
    iter->index++;
    iter->callback(list, iter);
}

void ZB_GameNodeList_Advancer(ZB_LinkedListView *list, ZB_LinkedListIterator *iter) {
    iter->listCurrent = (void*) ( ((ZB_GameNodeList*)iter->listCurrent)->next );
    iter->index++;
    iter->callback(list, iter);
}