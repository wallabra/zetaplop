/*************************************************/
/*          _            ___ _    _        _     */
/*  ____ __| |___ _ __  / / (_)__| |_ ___ | |_   */
/* |_ / '_ \ / _ \ '_ \/ /| | (_-<  _(_-<_| ' \  */
/* /__| .__/_\___/ .__/_/ |_|_/__/\__/__(_)_||_| */
/*    |_|        |_|                             */
/* Common interface for linked lists.            */
/*************************************************/
// Authored by Gustavo Rehermann <rehermann6046@gmail.com>.
// Licensed under MIT.

#ifndef lists_h
#define lists_h



struct ZB_LinkedListView;
struct ZB_LinkedListIterator;

typedef void *(*IteratorCallback)(struct ZB_LinkedListView *list, struct ZB_LinkedListIterator *iter);
typedef void (*IteratorAdvancer)(struct ZB_LinkedListView *list, struct ZB_LinkedListIterator *iter);

typedef struct ZB_LinkedListView {
    void *listRoot;
    IteratorAdvancer advancer;
} ZB_LinkedListView;

typedef struct ZB_LinkedListIterator {
    unsigned short index;
    void *listCurrent;
    IteratorCallback callback;
} ZB_LinkedListIterator;



#endif // lists.h